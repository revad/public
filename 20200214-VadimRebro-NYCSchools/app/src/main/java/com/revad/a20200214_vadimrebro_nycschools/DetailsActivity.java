package com.revad.a20200214_vadimrebro_nycschools;

//Application implements MVVM pattern, this a View class of that.

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R2.id.nameViewValue) TextView nameView;
    @BindView(R2.id.satReadingViewValue) TextView readingSATView;
    @BindView(R2.id.satWritingViewValue) TextView writingSATView;
    @BindView(R2.id.satMathViewValue) TextView mathSATView;
    @BindView(R2.id.locationViewValue) TextView locationView;
    @BindView(R2.id.phoneNumberViewValue) TextView phoneView;
    @BindView(R2.id.websiteViewValue) TextView websiteView;
    private String latValue;
    private String longValue;
    private SatViewModel satViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        //Extracting data passed from MainActivity to display information about selected school
        Bundle extras = getIntent().getExtras();
        School mSchool = extras.getParcelable("SCHOOL_DATA");

        nameView.setText(mSchool.getName());
        locationView.setText(mSchool.getLocation());
        phoneView.setText(mSchool.getPhoneNumber());
        websiteView.setText(mSchool.getUrl());
        latValue = mSchool.getLatitude();
        longValue = mSchool.getLongitude();
        String sDbNum = mSchool.getDbNum();

        //Getting the instance of ViewModel
        //Factory is used to change ViewModel so it was able to accept parameters on creating its instance
        //Parameter passed to ViewModel is schools DBN value from JSON
        satViewModel = ViewModelProviders.of(this, new SchoolSatViewModelFactory(this.getApplication(), sDbNum)).get(SatViewModel.class);

        //Adding observer to owner (DetailsActivity). If LiveData gets data/updates - observer will notify owner
        satViewModel.getSatData().observe(this, sats -> {
            readingSATView.setText(sats.getReadingSat());
            writingSATView.setText(sats.getWritingSat());
            mathSATView.setText(sats.getMathSat());
        });
    }

    //If a user clicks on school's address - a map app opens
    public void onLocationViewClick(View view) {
        Uri gmmIntentUri = Uri.parse("geo:" + latValue + "," + longValue + "?q=" + nameView.getText());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    //If a user clicks on school's phone number - a phone app opens
    public void onPhoneNumberViewClick(View view) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phoneView.getText().toString()));
        startActivity(callIntent);
    }

    //If a user clicks on school's website address - a browser app opens
    public void onWebsiteViewClick(View view) {
        Uri schoolUri = Uri.parse(websiteView.getText().toString());
        Intent websiteIntent = new Intent(Intent.ACTION_VIEW, schoolUri);
        startActivity(websiteIntent);
    }
}
