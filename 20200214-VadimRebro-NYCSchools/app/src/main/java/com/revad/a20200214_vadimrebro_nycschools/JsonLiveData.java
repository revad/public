package com.revad.a20200214_vadimrebro_nycschools;

//This is a Model class of MVVM pattern, it encapsulates app's data

import android.content.Context;
import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class JsonLiveData extends MutableLiveData<ArrayList<School>> {
    private final Context context;
    private static final String SCHOOLS_REQUEST_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";

    public JsonLiveData(Context context){
        this.context=context;
        LoadData();
    }

    private void LoadData() {
        //Creating request dispatch queue with thread pool of dispatchers
        final RequestQueue requestQueue = Volley.newRequestQueue(context);

        //Creating request for retrieving JSONArray response from website
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, SCHOOLS_REQUEST_URL, null,
            new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    ArrayList<School> schools = new ArrayList<>();
                    for(int i=0; i < response.length(); i++) {
                        try {
                            JSONObject obj=response.getJSONObject(i);

                            String mDbNum = obj.getString("dbn");
                            String mName = obj.getString("school_name");

                            String mLocation = obj.optString("location", "Not specified");
                            mLocation = correctPostalAddress(mLocation);

                            String mUrl = obj.optString("website", "Not specified");
                            mUrl = correctWebsiteAddress(mUrl);

                            String mLatitude = obj.optString("latitude", "0");
                            String mLongitude = obj.optString("longitude", "0");
                            String mPhoneNumber = obj.optString("phone_number", "Not specified");
                            String readingSAT = null;
                            String writingSAT = null;
                            String mathSAT = null;
                            schools.add(new School(mDbNum, mName, mLocation, mUrl, mLatitude, mLongitude, mPhoneNumber, readingSAT, writingSAT, mathSAT));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //Setting value of LiveData instance
                    setValue(schools);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("VolleyError", error.getMessage());
                }
            }
        );
        //Adding request to dispatch queue
        requestQueue.add(getRequest);
    }

    private String correctPostalAddress (String postalAddress) {
        if (postalAddress.contains("(")) {
            int bracePos = postalAddress.lastIndexOf("(");
            postalAddress = postalAddress.substring(0, bracePos-1);
        }
        return postalAddress;
    }

    //If website address is missing a "http://" header - we add it there
    private String correctWebsiteAddress (String websiteAddress) {
        if (!websiteAddress.startsWith("http:")) {
            websiteAddress = "http://" + websiteAddress;
        }
        return websiteAddress;
    }
}
