package com.revad.a20200214_vadimrebro_nycschools;

//Application implements MVVM pattern, this a View class of that.

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SchoolsAdapter.ListItemClickListener {

    private SchoolsAdapter mAdapter;
    private SchoolViewModel schoolViewModel;
    private ArrayList<School> schools;
    @BindView(R2.id.rv_schools) RecyclerView mSchoolsList;
    @BindView(R2.id.empty_state_view) TextView emptyStateView;
    @BindView(R2.id.spinner_widget) ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //If app has Internet access then load data
        if (isNetworkAvailable()) {
            spinner.setVisibility(View.VISIBLE);
            emptyStateView.setVisibility(View.GONE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            mSchoolsList.setLayoutManager(layoutManager);
            mSchoolsList.setHasFixedSize(true);
            schools = new ArrayList<School>();
            mAdapter = new SchoolsAdapter(this, schools);
            mSchoolsList.setAdapter(mAdapter);

            //Getting the instance of ViewModel
            schoolViewModel = ViewModelProviders.of(this).get(SchoolViewModel.class);

            //Adding observer to owner (MainActivity). If LiveData gets data/updates - observer will notify owner
            schoolViewModel.getSchoolsList().observe(this, schoolsList -> {
                spinner.setVisibility(View.GONE);
                mAdapter.setData(schoolsList);
            });
        }
        else {
            spinner.setVisibility(View.GONE);
            emptyStateView.setVisibility(View.VISIBLE);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    //Pass the selected school data to DetailsActivity
    @Override
    public void onListItemClick(School currentSchool) {
        Intent schoolDetailIntent;
        schoolDetailIntent = new Intent(MainActivity.this, DetailsActivity.class);
        schoolDetailIntent.putExtra("SCHOOL_DATA", currentSchool);
        startActivity(schoolDetailIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        if (!isNetworkAvailable()) menu.setGroupEnabled(0, false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //If Search menu item was clicked
        if (id == R.id.action_search) {

            final EditText txtUrl = new EditText(this);
            txtUrl.setHint(R.string.search_hint);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.search_title)
                    .setView(txtUrl)
                    .setPositiveButton(R.string.search_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String findSchool = txtUrl.getText().toString();
                            int i=0;

                            //Looking through the schools list and deleting those not matching the search criteria
                            while (i < mAdapter.getItemCount()) {
                                if (!mAdapter.getSchoolData(i).getName().toLowerCase().contains(findSchool.toLowerCase())) {
                                    mAdapter.removeSchool(i);
                                    i--;
                                }
                                i++;
                            }
                            mAdapter.update();
                        }
                    })
                    .setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    })
                    .show();

            return true;
        }

        if (id == R.id.action_reload) {
            //Reload is used to reset the search results to get back to the initial schools list
            if (mAdapter.isFiltered()) {
                mAdapter.deleteAllData();
                mAdapter.restoreData();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
