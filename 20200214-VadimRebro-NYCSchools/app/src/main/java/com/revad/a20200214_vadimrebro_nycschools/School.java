package com.revad.a20200214_vadimrebro_nycschools;

import android.os.Parcel;
import android.os.Parcelable;

public class School implements Parcelable {

    private String mDbNum;
    private String mLocation;
    private String mName;
    private String mUrl;
    private String mLatitude;
    private String mLongitude;
    private String mPhoneNumber;
    private String mReadingSAT;
    private String mWritingSAT;
    private String mMathSAT;

    //School class is used both for fetching schools list and school's SAT data, that's why its constructor includes all these parameters:
    public School (String dbNum, String name, String location, String url, String latitude, String longitude, String phoneNumber, String readingSAT, String writingSAT, String mathSAT) {
        mDbNum = dbNum;
        mName = name;
        mLocation = location;
        mUrl = url;
        mLatitude = latitude;
        mLongitude = longitude;
        mPhoneNumber = phoneNumber;
        mReadingSAT = readingSAT;
        mWritingSAT = writingSAT;
        mMathSAT = mathSAT;
    }

    protected School (Parcel in) {
        mDbNum = in.readString();
        mLocation = in.readString();
        mName = in.readString();
        mUrl = in.readString();
        mLatitude = in.readString();
        mLongitude = in.readString();
        mPhoneNumber = in.readString();
        mReadingSAT = in.readString();
        mWritingSAT = in.readString();
        mMathSAT = in.readString();
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mDbNum);
        parcel.writeString(mLocation);
        parcel.writeString(mName);
        parcel.writeString(mUrl);
        parcel.writeString(mLatitude);
        parcel.writeString(mLongitude);
        parcel.writeString(mPhoneNumber);
        parcel.writeString(mReadingSAT);
        parcel.writeString(mWritingSAT);
        parcel.writeString(mMathSAT);
    }

    public String getDbNum () { return mDbNum; }

    public String getName () { return mName; }

    public String getLocation () { return mLocation; }

    public String getUrl() { return mUrl; }

    public String getLatitude() { return mLatitude; }

    public String getLongitude() { return mLongitude; }

    public String getPhoneNumber() { return mPhoneNumber; }

    public String getReadingSAT() { return mReadingSAT; }

    public String getWritingSAT() { return mWritingSAT; }

    public String getmMathSAT() { return mMathSAT; }
}
