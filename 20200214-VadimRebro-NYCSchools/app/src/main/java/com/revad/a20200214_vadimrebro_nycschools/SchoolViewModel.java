package com.revad.a20200214_vadimrebro_nycschools;

//This is a ViewModel class of MVVM pattern

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import java.util.ArrayList;

public class SchoolViewModel extends AndroidViewModel {

    @Nullable
    private JsonLiveData schoolsList;

    public SchoolViewModel(@NonNull Application application) {
        super(application);
        if(schoolsList == null) {
            schoolsList = new JsonLiveData(application);
        }
    }

    public MutableLiveData<ArrayList<School>> getSchoolsList() {
        return schoolsList;
    }
}
