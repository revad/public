package com.revad.a20200214_vadimrebro_nycschools;

//This class implements the logic of RecyclerView adapter

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolsViewHolder> {

    private ArrayList<School> mSchoolData;
    private ArrayList<School> mSchoolDataBackup;
    final private ListItemClickListener mOnClickListener;
    private boolean filtered = false;

    public interface ListItemClickListener {
        void onListItemClick(School schoolData);
    }

    public SchoolsAdapter(ListItemClickListener listener, ArrayList<School> schools) {
        mOnClickListener = listener;
        mSchoolData = new ArrayList<School>();
        mSchoolDataBackup = new ArrayList<School>();
        mSchoolData.addAll(schools);
    }

    @Override
    public SchoolsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, viewGroup, false);
        SchoolsViewHolder viewHolder = new SchoolsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SchoolsViewHolder holder, int position) {
        holder.listItemView.setText(getSchoolData(position).getName());
    }

    public void setData(ArrayList<School> schoolList) {
        mSchoolData.addAll(schoolList);
        mSchoolDataBackup.addAll(schoolList);
        update();
    }

    @Override
    public int getItemCount() {
        return mSchoolData.size();
    }

    public School getSchoolData(int item) {
        return mSchoolData.get(item);
    }

    public void deleteAllData() {
        mSchoolData.clear();
        update();
    }

    public void restoreData() {
        filtered = false;
        mSchoolData.addAll(mSchoolDataBackup);
        update();
    }

    public void removeSchool(int itemIndex) {
        if (!filtered) filtered = true;
        mSchoolData.remove(itemIndex);
        update();
    }

    public boolean isFiltered() { return filtered; }

    public void update() { notifyDataSetChanged(); }

    class SchoolsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView listItemView;

        public SchoolsViewHolder(View itemView) {
            super(itemView);

            listItemView = (TextView) itemView.findViewById(R.id.nameView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(getSchoolData(clickedPosition));
        }
    }
}
